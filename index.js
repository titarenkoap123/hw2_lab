const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://Titarenko:mongoPSW@cluster0.5m91kxr.mongodb.net/todoapp?retryWrites=true&w=majority')

const {notesRouter} = require('./src/notesRouter.js');
const {authRouter} = require('./src/authRouter.js');
const {usersRouter} = require('./src/usersRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);

app.listen(8080);

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({message: 'Server error'});
}