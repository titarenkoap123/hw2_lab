const express = require('express');
const router = express.Router();
const {registerUser, loginUser} = require('./authService.js');
/* const {authMiddleware} = require('./middleware/authMiddleware.js'); */

router.post('/register', registerUser);
router.post('/login', loginUser);
/* router.get('/users/me', authMiddleware, getUser); */

module.exports = {
  authRouter: router,
} 