const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  }
}, {timestamps: {createdAt: "created Date"}});

const Note = mongoose.model('Note', noteSchema);

module.exports = {
  Note
}