const {User} = require('./models/Users.js')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const jwtKey = 'secret-jwt-key';

const registerUser = async (req, res, next) => {
  const {username, password} = req.body;

  const user = new User ({
    username,
    password: await bcrypt.hash(password, 10)
  });

  user.save()
    .then(saved => res.status(200).send({"message": "Success"}))
    .catch(err => {
      next(err);
    });
}

const loginUser = async (req, res, next) => {
  const user = await User.findOne({username: req.body.username});
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = {username: user.username, userId: user._id};
    const jwtToken = jwt.sign(payload, jwtKey);
    return res.json({"message": "Success", "jwt_token": jwtToken})
  }
  return res.status(400).json({"message": "No authorized"});
}

module.exports = {
  registerUser,
  loginUser,
} 