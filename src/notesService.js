const {Note} = require('./models/Notes.js');

function createNote (req, res, next) {
  const text = req.body.text;
  const note = new Note({
    text,
    userId: req.user.userId
  })
  note.save().then(saved => {
    res.status(200).send({'message': 'Success'});
  })
}

function getNotes (req, res, next) {
  Note.find().then(result => {
    if (req.query.offset || req.query.limit) {
      const startIndex = +req.query.offset - 1;
      const lastIndex = +req.query.limit + +startIndex;
      const resultObj = {
        "offset": req.query.offset,
        "limit": req.query.limit,
        "count": result.length,
        "notes": result.slice(startIndex, lastIndex)
      };
      return res.status(200).json(resultObj);
    } else {
      return res.status(200).json({
        "offset": 0,
        "limit": 0,
        "count": result.length,
        "notes": result
      });
    }
  });
}

function getNote (req, res, next) {
  Note.findById(req.params.id)
    .then((note) => res.json({"note": note}))
}

const updateNote = async (req, res, next) => {
  const note = await Note.findById(req.params.id);
  const text = req.body.text;

  if(text) {
    note.text = text;
  }

  return note.save().then(saved => {
    res.status(200).send({'message': 'Success'});
  })
}

const markNoteComplete = (req, res, next) => {
  return Note.findByIdAndUpdate({_id: req.params.id, userId: req.user.userId}, {$set: {completed: true}})
    .then(result => {
      res.status(200).send({'message': 'Success'});
    })
}

const deleteNote = async (req, res, next) => {
  try {
    await Note.findByIdAndDelete(req.params.id).then(()=> {
      res.status(200).send({'message': 'Success'});
    })
  } catch (err) {
    res.status(404).send({'message': `Note with id ${req.params.id} not found`});
  }
}

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  markNoteComplete,
  deleteNote
};