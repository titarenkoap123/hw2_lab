const express = require("express");
const router = express.Router();
const {getUser, changePassword, deleteUser} = require('./usersService.js');
const {authMiddleware} = require('./middleware/authMiddleware.js');

router.get('/me', authMiddleware, getUser);
router.patch('/me', authMiddleware, changePassword);
router.delete('/me', authMiddleware, deleteUser)

module.exports = {
  usersRouter: router
}