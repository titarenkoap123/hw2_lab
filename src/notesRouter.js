const express = require('express');
const router = express.Router();
const {createNote, getNotes, getNote, updateNote, markNoteComplete, deleteNote} = require('./notesService.js');
const {authMiddleware} = require('./middleware/authMiddleware.js');

router.post('/', authMiddleware, createNote);
router.get('/', authMiddleware, getNotes);
router.get('/:id', authMiddleware, getNote);
router.put('/:id', authMiddleware, updateNote);
router.patch('/:id', authMiddleware, markNoteComplete);
router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
}