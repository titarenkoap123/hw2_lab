const bcrypt = require('bcryptjs/dist/bcrypt');
const {User} = require('./models/Users');
const {Note} = require('./models/Notes.js');

const getUser = (req, res, next) => {
  User.findById(req.user.userId)
    .then(result => {
      res.status(200).send({"user": result});
    })
}

const changePassword = async (req, res, next) => {
  const user = await User.findById(req.user.userId);
  if(await bcrypt.compare(String(req.body.oldPassword), String(user.password))) {
    const newPassword = await bcrypt.hash(req.body.newPassword, 10);
    user.password = newPassword;
    return user.save().then(saved => res.status(200).send({"message": "Success"}))
  } else {
    return res.status(400).send({'message': 'Please enter correct password'})
  }
}

const deleteUser = async (req, res, next) => {
  await Note.deleteMany({userId: req.user.userId});
  return User.findByIdAndDelete(req.user.userId)
    .then(() => res.status(200).send({'message': 'Success'}))
}

module.exports = {
  getUser,
  changePassword,
  deleteUser
}